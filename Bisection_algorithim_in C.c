#include <stdio.h>
#include <math.h>

// This function will take the polynomial
float F(float x)
{
    // return x*x - 2;
    // return x*x*x*x*x*x*x -126; 
    // return x*x*x - 43;
    return x*x*x*x*x - 729;

}
 
// Algorithm is implemented
float bisect(float l, float r)
{
    float z, err = 0.0005;
    if (F(l)*F(r)>0)
    {
        printf("Re_run code and try another interval\n");
    }
    else 
    {
        while ((fabs(l-r))  > err)
        {
            z = (l + r)/2;
            if (F(l) * F(z) > 0)
            {
                l = z;
            }
            else
            {
                r = z;
            }
        }
        printf("The root is %.3f", z);
    }
}

int main()
{
    float l, r;
    printf("Enter your interval (l,r): \n");
    scanf("%f %f", &l, &r);
    bisect(l, r);

    return 0;
}

